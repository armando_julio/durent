<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Product extends Model
{
    protected $fillable = [
        'name', 'short', 'body', 'stocks', 'bookings',
    ];
    protected $dates = ['deleted_at'];
}
