<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Booking extends Model
{
	use SoftDeletes;

    protected $fillable = [
        'name', 'cc', 'product_id', 'quantity',
    ];
    protected $dates = ['deleted_at'];
}
