<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Booking;
use Carbon\Carbon;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $now = Carbon::now()->toDateTimeString();
        $today = Carbon::today();
        
        
        
        
        
        
        

        $products = \App\Product::paginate();
        $booking = Booking::where('expire_date', '>', $now)->get();
        $booking_atended = Booking::onlyTrashed()->get();
        $booking_expired = Booking::where('expire_date', '<=', $now)->get();
        $created_bookings = Booking::withTrashed()->count();
        $booking_atended_graph = Booking::onlyTrashed()->count();

        /*$booking_1 = Booking::onlyTrashed()->where('created_at','>=', $today->toDateTimeString())->count();
        $booking_2 = Booking::onlyTrashed()->where('created_at','>=', $yesterday)->count();
        $booking_3 = Booking::onlyTrashed()->where('created_at','>=', $three)->count();
        $booking_4 = Booking::onlyTrashed()->where('created_at','>=', $four)->count();
        $booking_5 = Booking::onlyTrashed()->where('created_at','>=', $five)->count();
        $booking_6 = Booking::onlyTrashed()->where('created_at','>=', $six)->count();
        $booking_7 = Booking::onlyTrashed()->where('created_at','>=', $seven)->count();
        $booking_8 = Booking::onlyTrashed()->where('created_at','>=', $eight)->count();*/

        $bookinga_1 = Booking::where('created_at','>=', $today->toDateTimeString())->count();
        $yesterday = $today->subDay()->toDateTimeString();
        $bookinga_2 = Booking::where('created_at','>=', $yesterday)->count();
        $three = $today->subDays(2)->toDateTimeString();
        $bookinga_3 = Booking::where('created_at','>=', $three)->count();
        $four = $today->subDays(3)->toDateTimeString();
        $bookinga_4 = Booking::where('created_at','>=', $four)->count();
        $five = $today->subDays(4)->toDateTimeString();
        $bookinga_5 = Booking::where('created_at','>=', $five)->count();
        $six = $today->subDays(5)->toDateTimeString();
        $bookinga_6 = Booking::where('created_at','>=', $six)->count();
        $seven = $today->subDays(6)->toDateTimeString();
        $bookinga_7 = Booking::where('created_at','>=', $seven)->count();
        $eight = $today->subDays(7)->toDateTimeString();
        $bookinga_8 = Booking::where('created_at','>=', $eight)->count();
        return view('home',compact('products','booking', 'booking_atended', 'booking_expired', 'created_bookings', 'booking_atended_graph',/* 'booking_1', 'booking_2', 'booking_3', 'booking_4', 'booking_5', 'booking_6', 'booking_7', 'booking_8',*/ 'bookinga_1', 'bookinga_2', 'bookinga_3', 'bookinga_4', 'bookinga_5', 'bookinga_6', 'bookinga_7', 'bookinga_8'));
    }

}
