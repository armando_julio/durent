<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Booking;
use App\Product;
use App\Http\Requests\Controllers;
use Carbon\Carbon;

class BookingController extends Controller
{
    public function index(){

    	$Bookings = Booking::orderBy('id', 'DESC')->paginate();
    	return view('bookings.index', compact('bookings'));
    }

    public function create(){
        //count active bookings
        $count2 = Booking::where('expire_date', '>', Carbon::now()->toDateTimeString())->count();
        $prd= Product::find(1);
        $prd->bookings = $count2;
        $prd->save();
    	$product = Product::all();
        return view('bookings.create', compact('product'));
    }

    public function store(Request $request){

        $booking = new Booking;
        $booking_info = $request->all();
		$now = Carbon::now();

        $booking->name = $booking_info['name'];
        $booking->cc = $booking_info['cc'];
        $booking->product_id = $booking_info['product_id'];
        $booking->quantity = $booking_info['quantity'];
        $booking->expire_date = $now->addHours(2); 

        $booking->save();

        $booking = Booking::whereCc($booking_info['cc'])->first();

        return view('bookings.show', compact('booking'));
    }

    public function edit($id){

        $product = Product::find($id);
        return view('products.edit', compact('product'));
    }

    public function update(Request $request, $id){

        $product = Product::find($id);

        $product->name = $request->name;
        $product->short = $request->short;
        $product->body = $request->body;
        
        $product->save();

        return redirect()->route('home');
    }

    public function show(Request $request, $cc){
		if ($request->has('cc')) {
            $count = Booking::where('cc','=',$request->input('cc'))
            ->count();
            if ($count) {
                $booking = Booking::whereCc($request->input('cc'))->orderBy('created_at', 'desc')->first();
            }
    		else {
                $data =['message' =>'Ésta cédula no tiene reservas. ¡Reserva ya!', 'cc'=> 'consult' ];
                return view('bookings.check', $data);
            }
		}
    	else {
    		$data =['message' =>'Ingresa tu cédula.'];
            return view('bookings.check', compact('booking'));
    	}
    	return view('bookings.show', compact('booking'));
    }

    public function destroy($id){
    	
    	$booking = Booking::find($id);
    	$booking->delete();
        //decrement booking
        Product::whereId($booking->product_id)->decrement('bookings', $booking->quantity);

    	return back()->with('message', 'Reserva atendida.');
    }
    
    public function check(){
    	$cc = "consult";
		return view('bookings.check', compact('cc'));
    }

    public function unique(Request $request){

        //Request inputs
        $booking_info = $request->all();

        //Check if there is stock
        $product = Product::whereId($booking_info['product_id'])->get();

        if ($product[0]->bookings < $product[0]->stock) {

            //Check if user already has a booking
            $count = Booking::where('expire_date', '>', Carbon::now()->toDateTimeString())->where('cc','=', $booking_info['cc'])
            ->count();

            $product = Product::all();
            $data =['message2' =>'Ésta cédula tiene una reserva activa. Consulta', 'product'=>$product ];

            if($count) {
                return view('bookings.create', $data);
            }

            else {

            //Save the Booking
            $booking = new Booking;
            $now = Carbon::now();

            $booking->name = $booking_info['name'];
            $booking->cc = $booking_info['cc'];
            $booking->product_id = $booking_info['product_id'];
            $booking->quantity = $booking_info['quantity'];
            $booking->expire_date = $now->addHours(2); 
            $booking->save();

            //Icrement the booking number of product
            Product::whereId($booking_info['product_id'])->increment('bookings', $booking_info['quantity']);

            //Variable for view
            $booking = Booking::whereCc($booking_info['cc'])->orderBy('created_at', 'desc')->first();

            return view('bookings.show', compact('booking'));
            }
        }
        else {
            $product = Product::all();
            $data =['message' =>'Lo sentimos, las reservas están llenas por éste momento. Intente más tarde, por favor.', 'product'=>$product ];
            return view('bookings.create', $data);
        }

    }

}
