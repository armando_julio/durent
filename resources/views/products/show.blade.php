@extends('layout')

@section('content')
	<div class="col-md-6 col-md-offset-3">
		<h2>
			{{ $product->name }}
			@if (Auth::check())
			<a href="{{ route('products.edit', $product->id) }}" class="btn btn-default pull-right" style="background-color: #aad697">editar</a>
			@else
			<a href="{{ route('products.index') }}" class="btn btn-default pull-right" style="background-color: #aad697">Listado</a>
			@endif
		</h2>
		<p>
			{{ $product->short }}
		</p>
		{!! $product->body !!}
	</div>
@endsection