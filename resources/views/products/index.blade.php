@extends('layout')

@section('content')
	<div class="col-md-6 col-md-offset-3">
		<h2>
			Listado de Productos
			@if (Auth::check())
				<a href="{{ route('products.create') }}" class="btn btn-default pull-right" style="background-color: #aad697">Nuevo</a>
			@else
				<a href="{{ url('/') }}" class="btn btn-default pull-right" style="background-color: #aad697">Inicio</a>
			@endif
		</h2>
		<table class="table table-hover table-striped">
			<thead>
				<tr>
					<th width="20px">ID</th>
					<th>Nombre del producto</th>
					<th colspan="3">&nbsp;</th>
				</tr>
			</thead>
			<tbody>
				@foreach($products as $product)
					<tr>
						<td>{{ $product->id }}</td>
						<td>
							<strong>{{ $product->name }}</strong>
							{{ $product->short }}
						</td>
						<td>
							<a href="{{ route('products.show', $product->id) }}" class="btn btn-link">ver</a>
						</td>
						@if (Auth::check())
						<td>
							<a href="{{ route('products.edit', $product->id) }}" class="btn btn-link">editar</a>
						</td>
						<td>
						<form action="{{ route('products.destroy', $product->id) }}" method="POST">
							{{ csrf_field() }}
							<input type="hidden" name="_method" value="DELETE">
							<button class="btn btn-link">borrar</button>
						</form>
						</td>
						@endif
					</tr>
				@endforeach
			</tbody>
		</table>
		{!! $products->render() !!}
	</div>
@endsection