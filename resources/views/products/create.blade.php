@extends('layout')

@section('content')
	<div class="col-sm-6 col-md-offset-3">
		<h2>
			Nuevo Producto
			<a href="{{ route('products.index') }}" class="btn btn-default pull-right" style="background-color: #aad697">Listado</a>
		</h2>
		{!! Form::open(['route' => 'products.store']) !!}
			
			@include('products.fragment.form')
			
		{!! Form::close() !!}
	</div>

	

@endsection