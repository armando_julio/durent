@extends('layouts.app')

@section('content')
<div class="container home">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Tabla de Productos DuRent</div>

                <div class="panel-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif
                    <div style="text-align: center;">
                    <a style="margin: 2px 5px 0px 5px;" href="{{ route('products.create') }}" class="btn btn-success ">Agregar producto</a>
                    <a style="margin: 2px 5px 0px 5px;" href="{{ route('bookings.create') }}" class="btn btn-success ">Nueva reserva</a>
                    <a style="margin: 2px 5px 0px 5px;" href="{{ route('bookings.check') }}" class="btn btn-success">Consultar reserva individual</a>
                    </div>
                    <table class="table table-hover table-striped">
                        <thead>
                            <tr>
                                <th width="20px">ID</th>
                                <th>Nombre del Producto</th>
                                <th colspan="3">&nbsp;</th>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach($products as $product)
                            <tr>
                                <td>{{ $product->id }}</td>
                                <td>
                                    <strong>{{ $product->name }}</strong>
                                    {{ $product->short }}
                                </td>
                                <td>
                                    <a href="{{ route('products.show', $product->id) }}" class="btn btn-link">Ver</a>
                                </td>
                                <td>
                                    <a href="{{ route('products.edit', $product->id) }}" class="btn btn-link">Editar</a>
                                </td>
                                <td>
                                   <form action="{{ route('products.destroy', $product->id) }}" method="POST">
                                        {{ csrf_field() }}
                                        <input type="hidden" name="_method" value="DELETE">
                                        <button class="btn btn-link">Borrar</button>
                                    </form> 
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    {!! $products->render() !!}
                </div>
            </div>
        </div>
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Reservas activas</div>
                <table class="table table-hover table-striped">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Nombre</th>
                            <th>Cedula</th>
                            <th>Producto</th>
                            <th>Cantidad</th>
                            <th>Creacion</th>
                            <th>Vencimiento</th>
                            <th>Atender</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($booking as $item)
                            <tr>
                                <td>{{ $item->id}}</td>
                                <td>{{ $item->name }}</td>
                                <td>{{ $item->cc}}</td>
                                @if($item->product_id = 1)
                                <p class="hidden">{{$item->product_name = 'Calculadora'}}} </p>
                                <td>{{ $item->product_name}}</td>
                                @else
                                <td>{{ $item->product_id}}</td>
                                @endif
                                <td>{{ $item->quantity}}</td>
                                <td>{{ $item->created_at}}</td>
                                <td>{{ $item->expire_date}}</td>
                                <td>
                                   <form action="{{ route('bookings.destroy', $item->id) }}" method="POST">
                                    {{ csrf_field() }}
                                    <input type="hidden" name="_method" value="DELETE">
                                    <button class="btn btn-success">Atender</button>
                                    </form> 
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>                
            </div>
        </div>
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Reservas atendidas</div>
                <table class="table table-hover table-striped">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Nombre</th>
                            <th>Cedula</th>
                            <th>Producto</th>
                            <th>Cantidad</th>
                            <th>Creacion</th>
                            <th>Vencimiento</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($booking_atended as $item)
                            <tr>
                                <td>{{ $item->id}}</td>
                                <td>{{ $item->name }}</td>
                                <td>{{ $item->cc}}</td>
                                @if($item->product_id = 1)
                                <p class="hidden">{{$item->product_name = 'Calculadora'}}} </p>
                                <td>{{ $item->product_name }}</td>
                                @else
                                <td>{{ $item->product_id}}</td>
                                @endif
                                <td>{{ $item->quantity}}</td>
                                <td>{{ $item->created_at}}</td>
                                <td>{{ $item->expire_date}}</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>                
            </div>
        </div>
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Reservas vencidas</div>
                <table class="table table-hover table-striped">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Nombre</th>
                            <th>Cedula</th>
                            <th>Producto</th>
                            <th>Cantidad</th>
                            <th>Creacion</th>
                            <th>Vencimiento</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($booking_expired as $item)
                            <tr>
                                <td>{{ $item->id}}</td>
                                <td>{{ $item->name }}</td>
                                <td>{{ $item->cc}}</td>
                                @if($item->product_id = 1)
                                <p class="hidden">{{$item->product_name = 'Calculadora'}}} </p>
                                <td>{{ $item->product_name }}</td>
                                @else
                                <td>{{ $item->product_id}}</td>
                                @endif
                                <td>{{ $item->quantity}}</td>
                                <td>{{ $item->created_at}}</td>
                                <td>{{ $item->expire_date}}</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>                
            </div>
        </div>
    </div>
</div>
@endsection
@section('addedjs')
<div style="text-align: center;">
    <h3>Gráficos</h3>
    <br/>
    <span style="font-weight:bold;" >Reservas creadas VS. Reservas atendidas</span>
    <canvas id="myChart" width="400" height="150"></canvas>
    <br/>
    <br/>
    <span style="font-weight:bold;" >Acumulado reservas creadas y no atendidas</span>
    <div style="max-width: 700px; margin: auto;">
    <canvas id="myLineChart" width="400" height="300"></canvas>
    </div>
    <br/>
    <br/>
    <!--span style="font-weight:bold;" >Tendencia reserva creadas</span-->
    <div style="max-width: 800px; margin: auto; padding-right: 15%">
    <canvas id="myBarChart" width="400" height="300"></canvas>
    </div>
</div>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.1/Chart.min.js"></script>
<script>
var ctx = document.getElementById("myChart");
var myChart = new Chart(ctx, {
    type: 'pie',
    data: {
        
    datasets: [{
        data: [{{$created_bookings}}, {{$booking_atended_graph}}],
                    backgroundColor: [
                '#02a3d1',
                '#55b24f'
            ],
    }],

    // These labels appear in the legend and in the tooltips when hovering different arcs
    labels: [
        'Reservas creadas',
        'Reservas atendidas'
    ]

    },
    options: {
        layout: {
            padding: {
                left: 10,
                right: 10,
                top: 10,
                bottom: 10
            }
        }
    }
});
var ctx = document.getElementById("myLineChart");
var myLineChart = new Chart(ctx, {
    type: 'line',
    data: 
    {
        labels: ["Hace una semana", "Hace seis días", "Hace cinco días", "Hace cuatro días", "Hace tres días", "Hace dos días", "Ayer", "Hoy"],
        datasets: [{
            label: '# reservas',
            data: [{{$bookinga_1}}, {{$bookinga_2}}, {{$bookinga_3}}, {{$bookinga_4}}, {{$bookinga_5}}, {{$bookinga_6}}, {{$bookinga_7}}, {{$bookinga_8}}],
            backgroundColor: [
                '#55b24f9c'
            ],
            borderColor: [
            '#55b24f9c'
            ],
            pointBackgroundColor:[
            '#55b24f',
            '#55b24f',
            '#55b24f',
            '#55b24f'
            ]
        }],
        
    },
    options: {
        layout: {
            padding: {
                left: 10,
                right: 10,
                top: 10,
                bottom: 10
            }
        }
    }
});
/*var ctx = document.getElementById("myBarChart");
var myBarChart = new Chart(ctx, {
    type: 'horizontalBar',
    data: {
        labels: ["Hace una semana", "Hace seis días", "Hace cinco días", "Hace cuatro días", "Hace tres días", "Hace dos días", "Ayer", "Hoy"],
        datasets: [{
            label: '# reservas',
            data: [12, 19, 3, 5, 4, 3, 4, 7],
            backgroundColor: [
                'rgba(255, 99, 132, 0.2)',
                'rgba(54, 162, 235, 0.2)',
                'rgba(255, 206, 86, 0.2)',
                'rgba(75, 192, 192, 0.2)',
                'rgba(153, 102, 255, 0.2)',
                'rgba(255, 159, 64, 0.2)',
                'rgba(75, 192, 192, 0.2)',
                'rgba(153, 102, 255, 0.2)'
            ],
            borderColor: [
                'rgba(255,99,132,1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)',
                'rgba(75, 192, 192, 1)',
                'rgba(153, 102, 255, 1)',
                'rgba(255, 159, 64, 1)',
                'rgba(75, 192, 192, 1)',
                'rgba(153, 102, 255, 1)'
            ],
            borderWidth: 1
        }]
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero:true
                }
            }]
        },
        layout: {
            padding: {
                left: 50,
                right: 0,
                top: 10,
                bottom: 10
            }
        }
    }
});*/
</script>


@endsection