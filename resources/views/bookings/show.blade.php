@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            <div class="panel panel-default">
                <div class="panel-heading">Consulta tu reserva</div>

                    @if(isset($message))
                        {{$message}}
                    @endif

                <div class="panel-body">
                <div>Recuerda, el formato de hora es 24h.</div> <br>
                    <table class="table table-hover table-striped">
                        <tr>
                            <td>Nombre</td>
                            <td>{{ $booking->name }} </td>
                        </tr>
                        <tr>
                            <td>Cedula</td>
                            <td>{{ $booking->cc }}</td>
                        </tr>
                        <tr>
                            <td>Cantidad</td>
                            <td> {{ $booking->quantity }}</td>
                        </tr>
                        <td>vence</td>
                        <td> {{ $booking->expire_date }}</td>
                    </table> 
                    <hr style="border-width: 4px; border-top: 3px solid #aad697";/>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection