@extends('layouts.app')

@section('content')
	<div class="col-md-6 col-md-offset-3">
		<h2>
			Nueva Reserva
			<a href="{{ route('products.index') }}" class="btn btn-default pull-right" style="background-color: #aad697">Listado</a>
		</h2>
		@if(isset($message))
		{{$message}}
		@endif
		@if(isset($message2))
		{{$message2}} <a href="{{ route('bookings.check') }}">aquí</a>
		@endif
		{!! Form::open(['route' => 'bookings.unique', 'method'=>'get']) !!}
			
			@include('bookings.fragment.form')
			
		{!! Form::close() !!}
	</div>


@endsection