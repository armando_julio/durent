<div class="form-group">
	{!! Form::label('name', 'Nombre') !!}
	{!! Form::text('name', null, ['class' => 'form-control']) !!}
</div>
<div class="form-group">
	{!! Form::label('cc', 'Cedula de Ciudadania') !!}
	{!! Form::text('cc', null, ['class' => 'form-control']) !!}
</div>
<div class="form-group">
	{!! Form::label('product_id', 'Producto') !!}
	{!! Form::select('product_id', [ 1 => $product[0]["name"] ]) !!}
</div>
<div class="form-group">
	{!! Form::label('quantity', 'Unidades') !!}
	{!! Form::select('quantity', [1 => 1, 2 => 2 ]) !!}
	
</div>
<div class="form-group">
	{!! Form::submit('Reservar', ['class' => 'btn btn-default btn-lg btn-block ', 'style'=>'background-color: #aad697']) !!}
</div>