@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            <div class="panel panel-default">
                <div class="panel-heading">Consulta tu reserva</div>
                <div class="panel-body">
                    @if(isset($message))
                    {{$message}}
                    @endif
                    {!! Form::open(['route' => ['bookings.show', $cc], 'method'=>'get']) !!}
                        @include('bookings.fragment.form_check')
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection