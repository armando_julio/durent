<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::resource('products', 'ProductController');
Auth::routes();

Route::get('home', 'HomeController@index')->name('home');
Route::resource('bookings', 'BookingController');
Route::get('check', 'BookingController@check')->name('bookings.check');
Route::get('unique', 'BookingController@unique')->name('bookings.unique');