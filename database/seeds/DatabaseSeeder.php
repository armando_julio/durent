<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(ProductsTableSeeder::class);

        DB::table('users')->insert([
            'name' => 'admin',
            'email' => 'nathaliemercado01@gmail.com',
            'password' => bcrypt('admin'),
        ]);

    }
}
